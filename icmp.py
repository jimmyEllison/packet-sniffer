import struct


class ICMP:

    def __init__(self, raw_data):
        self.icmp_type, self.code, self.checksum = struct.unpack("!BBH", raw_data[:4])
        self.data = raw_data[4:]
