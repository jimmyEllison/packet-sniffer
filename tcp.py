import struct


class TCP:

    def __init__(self, raw_data):
        self.src_port, self.dest_port, self.sequence, self.acknowledgement, offset_reserve_flag = struct.unpack("!HHLLH", raw_data[:14])
        offset = (offset_reserve_flag >> 12) * 4
        self.flag_urg = (offset_reserve_flag & 32) >> 5
        self.flag_ack = (offset_reserve_flag & 16) >> 4
        self.flag_psh = (offset_reserve_flag & 8) >> 3
        self.flag_rst = (offset_reserve_flag & 4) >> 2
        self.flag_syn = (offset_reserve_flag & 2) >> 1
        self.flag_fin = offset_reserve_flag & 1
        self.data = raw_data[offset:]
