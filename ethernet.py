import socket
import struct


class Ethernet:

    def __init__(self, raw_data):
        dest, src, proto = struct.unpack("!6s6sH", raw_data[:14])
        self.dest_mac = self.get_mac_addr(dest)
        self.src_mac = self.get_mac_addr(src)
        self.proto = socket.htons(proto)
        self.data = raw_data[14:]


    def get_mac_addr(self, raw_mac):
        bytes_str = map("{:02x}".format, raw_mac)
        return ":".join(bytes_str).upper()
