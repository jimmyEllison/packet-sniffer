import socket
import struct
import textwrap
from ethernet import Ethernet
from ipv4 import IPv4
from icmp import ICMP
from tcp import TCP
from udp import UDP
from pcap import Pcap


TAB_1 = "\t - "
TAB_2 = "\t\t - "
TAB_3 = "\t\t\t - "

DATA_TAB_1 = "\t   "
DATA_TAB_2 = "\t\t   "
DATA_TAB_3 = "\t\t\t   "


# format muli-line data
def format_multi_line(prefix, string, size=80):
    size -= len(prefix)
    if isinstance(string, bytes):
        string = " ".join(r"{:02x}".format(byte) for byte in string)
        if size % 2:
            size -= 1
    return "\n".join([prefix + line for line in textwrap.wrap(string, size)])


# main program
def main():
    pcap = Pcap("capture.pcap")
    connection = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))

    while True:
        raw_data, addr = connection.recvfrom(65536)
        pcap.write(raw_data)
        eth = Ethernet(raw_data)
        print("Ethernet Frame:")
        print("{}Destination: {}, Source: {}, Protocol: {}".format(TAB_1, eth.dest_mac, eth.src_mac, eth.proto))
        if eth.proto == 8:
            ipv4 = IPv4(eth.data)
            print("{}IPv4 Packet:".format(TAB_1))
            print("{}Version: {}, Header Length: {}, TTL: {}".format(TAB_2, ipv4.version, ipv4.header_length, ipv4.ttl))
            print("{}Protocol: {}, Source: {}, Target: {}".format(TAB_2, ipv4.proto, ipv4.src, ipv4.target))
            if ipv4.proto == 1:
                icmp = ICMP(ipv4.data)
                print("{}ICMP Packet:".format(TAB_1))
                print("{}Type: {}, Code: {}, Checksum: {}".format(TAB_2, icmp.icmp_type, icmp.code, icmp.checksum))
                print("{}ICMP Data:".format(TAB_2))
                print(format_multi_line(DATA_TAB_3, icmp.data))

            elif ipv4.proto == 6:
                tcp = TCP(ipv4.data)
                print("{}TCP Segment:".format(TAB_1))
                print("{}Source port: {}, Destination port: {}".format(TAB_2, tcp.src_port, tcp.dest_port))
                print("{}Sequence: {}, Acknowledgement: {}".format(TAB_2, tcp.sequence, tcp.acknowledgement))
                print("{}Flags:".format(TAB_2))
                print("{}URG: {}, ACK: {}, PSH: {},".format(TAB_3, tcp.flag_urg, tcp.flag_ack, tcp.flag_psh))
                print("{}RST: {}, SYN: {}, FIN: {}".format(TAB_3, tcp.flag_rst, tcp.flag_syn, tcp.flag_fin))
                print("{}TCP Data:".format(TAB_2))
                print(format_multi_line(DATA_TAB_3, tcp.data))

            elif ipv4.proto == 17:
                udp = UDP(ipv4.data)
                print("{}UDP Segment:".format(TAB_1))
                print("{}Source port: {}, Destination port: {}, Length: {}".format(TAB_2, udp.src_port, udp.dest_port, udp.size))

            else:
                print("Other IPv4 Data:".format(TAB_1))
                print(format_multi_line(DATA_TAB_2, ipv4.data))

        else:
            print("Ethernet Data:")
            print(format_multi_line(DATA_TAB_1, eth.data))

    pcap.close()


if __name__ == "__main__":
    main()
