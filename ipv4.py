import struct


class IPv4:

    def __init__(self, raw_data):
        version_header_length = raw_data[0]
        self.version = version_header_length >> 4
        self.header_length = (version_header_length & 15) * 4
        self.ttl, self.proto, src, target = struct.unpack("!8xBB2x4s4s", raw_data[:20])
        self.src = self.format_ipv4(src)
        self.target = self.format_ipv4(target)
        self.data = raw_data[self.header_length:]


    def format_ipv4(self, ipv4_addr):
        return ".".join(map(str, ipv4_addr))
